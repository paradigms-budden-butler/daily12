// Bradley Budden (bbudden) & Megan Butler (mbutle13) - Daily12
// JS for index.html

var submitButton = document.getElementById("bsr-submit-button");
submitButton.onmouseup = getFormInfo;

function getFormInfo(){

    var title_text = document.getElementById("input-text-title").value;
    var director_text = document.getElementById("input-text-director").value;
    var summary_text = document.getElementById("text-area-summary").value;

    var genres_string = '';
    if (document.getElementById("checkbox-action").checked) {
        genres_string += '- Action<br>';
    }
    if (document.getElementById("checkbox-horror").checked){
        genres_string += '- Horror<br>';
    }
    if (document.getElementById("checkbox-comedy").checked){
        genres_string += '- Comedy';
    }
    
    var movie_dict = {};
    movie_dict['title'] = title_text;
    movie_dict['director'] = director_text;
    movie_dict['summary'] = summary_text;
    movie_dict['genres'] = genres_string;
    displayInfo(movie_dict);
}

function displayInfo(movie_dict){
    var details_panel = document.getElementById("panel-details");
    var summary_panel = document.getElementById("panel-summary");

    details_panel.innerHTML = '<em>' + movie_dict['title'] + '</em>' + " directed by " + movie_dict['director'] 
                              + '<br>' + '<br>Genres:<br>' + movie_dict['genres'];

    summary_panel.innerHTML = movie_dict['summary'];
}